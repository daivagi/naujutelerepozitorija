package com.my.name.api.Repozitorijos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.my.name.api.Dovanos.Dovana;

public interface DovanosRepozitorija extends JpaRepository<Dovana, Long> {

}
