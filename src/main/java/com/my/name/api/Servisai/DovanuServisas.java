package com.my.name.api.Servisai;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.my.name.api.Dovanos.Dovana;
import com.my.name.api.Dovanos.NaujaDovana;
import com.my.name.api.Repozitorijos.DovanosRepozitorija;


@Service
public class DovanuServisas {
		
		private DovanosRepozitorija dovanosRepozitorija;

		@Autowired
		public DovanuServisas(DovanosRepozitorija productRepository) {		
			this.dovanosRepozitorija = productRepository;
		}
		
		@Transactional(readOnly = true)
		public List<Dovana> getDovanas() {
			return dovanosRepozitorija.findAll(); 
		}
		
		@Transactional(readOnly = true)
		public Dovana getDovana(Long id) {
			return getDovanas().stream().filter(p -> p.getId().equals(id)).findFirst()
					.orElseThrow(() -> new RuntimeException("Can't find product")); // ProductForClient
		}	
		@Transactional
		public Dovana updateDovana(Long id, NaujaDovana naujaDovana) {
			Dovana existingDovana = getDovana(id);
			existingDovana.setPavadinimas(naujaDovana.getPavadinimas());
			existingDovana.setAprasymas(naujaDovana.getAprasymas());
			existingDovana.setVaikas(naujaDovana.getVaikas());
			existingDovana.setPaveiksliukas(naujaDovana.getPaveiksliukas());			
			return existingDovana;
		}

		
		@Transactional
		public void createDovana(NaujaDovana naujaDovana) {
			Dovana dovana = new Dovana();
			dovana.setPavadinimas(naujaDovana.getPavadinimas());
			dovana.setAprasymas(naujaDovana.getAprasymas());
			dovana.setVaikas(naujaDovana.getVaikas());
			dovana.setPaveiksliukas(naujaDovana.getPaveiksliukas());				
			dovanosRepozitorija.save(dovana);
		}
		
		@Transactional
		public void removeDovana(Long productID) {

			dovanosRepozitorija.deleteById(productID);
			
		}

		

	}


