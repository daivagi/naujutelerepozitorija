package com.my.name.api.Kontroleriai;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.my.name.api.Dovanos.Dovana;
import com.my.name.api.Dovanos.NaujaDovana;
import com.my.name.api.Servisai.DovanuServisas;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(value = "dovana")
@RequestMapping(value = "/api/dovanos")
public class DovanosKontroleris {

		public DovanosKontroleris() {}
		
			@Autowired		
			private DovanuServisas dovanuServisas;
					
					
			@Autowired
			public DovanosKontroleris(DovanuServisas dovanuServisas){ 
				this.dovanuServisas = dovanuServisas;     
				}
			
			@RequestMapping(method = RequestMethod.GET)
			@ApiOperation(value="Gauti dovanas",notes="Parodo dovanas")
			public List<Dovana> getProducts(){
				return dovanuServisas.getDovanas();
			}	

			@RequestMapping(path = "/{productID}", method = RequestMethod.GET)
			@ApiOperation(value="Gauti dovana",notes="Parodo dovana pagal id")
			public Dovana getDovana(@PathVariable Long productId){
				return dovanuServisas.getDovana(productId);					
			}	
			
			/* Sukurs vartotoją ir grąžins atsakymą su HTTP statusu 201 */
			@RequestMapping(method = RequestMethod.POST)
			@ApiOperation(value = "Sukurti dovana", notes = "Sukurti dovana pagal duomenis")
			@ResponseStatus(HttpStatus.CREATED)
			public void createDovana(
					@ApiParam(value = "Dovanos info", required = true) @Valid @RequestBody final NaujaDovana naujaDovana) {
				dovanuServisas.createDovana(naujaDovana);				

			}
			
			@RequestMapping(path = "/{productID}", method = RequestMethod.PUT)
			@ApiOperation(value = "Atnaujinti dovanos info", notes = "Atnaujina dovana")
			public Dovana updateDovana(@ApiParam(value = "Product Data", required = true) @Valid @PathVariable Long productID,
					@RequestBody final NaujaDovana naujaDovana) {
				return dovanuServisas.updateDovana(productID, naujaDovana);

			}
			@RequestMapping(path = "/{productID}", method = RequestMethod.DELETE)
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void removeDovana(@PathVariable final Long productID ) {
				dovanuServisas.removeDovana(productID);
				
			}
			
	}
	

