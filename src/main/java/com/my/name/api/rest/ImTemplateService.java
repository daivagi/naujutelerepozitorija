package com.my.name.api.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.my.name.persitence.domain.ImTemplateEntity;
import com.my.name.persitence.repositories.ImTemplateRepository;


//Logic goes here!!!!
@Service
public class ImTemplateService {
	@Autowired
	private ImTemplateRepository templateRepository;

	
	public List<ImTemplateEntity> retrieveAllTemplates() {
		return templateRepository.findAll();
	}
	
	public ImTemplateEntity createNewTemplate(ImTemplateEntity templateEntity) {
		return templateRepository.save(templateEntity);
		
	}
}
