package com.my.name.api.Dovanos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Dovana {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String pavadinimas;
	private String aprasymas;
	private String paveiksliukas;
	private Boolean vaikas;
	
	public Dovana () {}
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPavadinimas() {
		return pavadinimas;
	}
	public void setPavadinimas(String pavadinimas) {
		this.pavadinimas = pavadinimas;
	}
	public String getAprasymas() {
		return aprasymas;
	}
	public void setAprasymas(String aprasymas) {
		this.aprasymas = aprasymas;
	}
	public String getPaveiksliukas() {
		return paveiksliukas;
	}
	public void setPaveiksliukas(String paveiksliukas) {
		this.paveiksliukas = paveiksliukas;
	}
	public Boolean getVaikas() {
		return vaikas;
	}
	public void setVaikas(Boolean vaikas) {
		this.vaikas = vaikas;
	}
	
	
}
