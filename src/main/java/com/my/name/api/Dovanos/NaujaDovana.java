package com.my.name.api.Dovanos;

public class NaujaDovana {

	private String pavadinimas;
	private String aprasymas;
	private String paveiksliukas;
	private Boolean vaikas;
	public String getPavadinimas() {
		return pavadinimas;
	}
	public void setPavadinimas(String pavadinimas) {
		this.pavadinimas = pavadinimas;
	}
	public String getAprasymas() {
		return aprasymas;
	}
	public void setAprasymas(String aprasymas) {
		this.aprasymas = aprasymas;
	}
	public String getPaveiksliukas() {
		return paveiksliukas;
	}
	public void setPaveiksliukas(String paveiksliukas) {
		this.paveiksliukas = paveiksliukas;
	}
	public Boolean getVaikas() {
		return vaikas;
	}
	public void setVaikas(Boolean vaikas) {
		this.vaikas = vaikas;
	}
	
	
}
