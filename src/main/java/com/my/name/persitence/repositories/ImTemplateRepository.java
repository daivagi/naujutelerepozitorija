package com.my.name.persitence.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.my.name.persitence.domain.ImTemplateEntity;

public interface ImTemplateRepository extends JpaRepository<ImTemplateEntity, Long>{

}
