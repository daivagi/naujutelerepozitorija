package com.my.name.persitence.controllers;

import com.my.name.api.rest.ImTemplateService;
import com.my.name.persitence.domain.ImTemplateEntity;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/template")
@Api (value = "template")
public class ImTemplateController {

	@Autowired
	private ImTemplateService service;
	
	@GetMapping("/all")
	@ApiOperation(value="Get templates",notes="Returns all registered templates")
	public List<ImTemplateEntity> listAll() {
		return service.retrieveAllTemplates();
	}

	
	@PostMapping("/post")
	@ApiOperation(value="post",notes="post")
	@Transactional
	public ResponseEntity<String> verifyUserExists(@RequestBody ImTemplateEntity template) {
		if (service.retrieveAllTemplates().isEmpty()) { //creates a new template entity ONLY if there are no templates on the database
		service.createNewTemplate(template);
		return new ResponseEntity<String>("Saved succesfully", HttpStatus.CREATED);
		}
		else 
		return new ResponseEntity<String>("Failed to create user", HttpStatus.CONFLICT);
	}
	
}
